﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateNPC : MonoBehaviour
{
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Camera")
        {
            col.transform.Rotate(0f, Random.Range(90f, 270f), 0f);
        }
    }
}