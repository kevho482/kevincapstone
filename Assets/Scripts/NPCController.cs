﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    public int size;
    public GameObject npcs;
    public GameObject goal;
    private GameObject[] objects;

    // Use this for initialization
    void Start()
    {
        //spawns a number of npcs on the map randomly
        Vector3 randPosition;
        int index = size;

        while (index >= 0)
        {
            randPosition = new Vector3(Random.Range(-50.0f, 50.0f), 1.0f, Random.Range(-50.0f, 50.0f));

            //checks to make sure things arent being spawned inside each other
            if (Physics.OverlapSphere(randPosition, 1.0f)[0].gameObject.name != "Camera" &&
                Physics.OverlapSphere(randPosition, 1.0f)[0].gameObject.name != "Player")
            {
                //spawns goal after all the npcs
                if (index == 0)
                    Instantiate(goal, randPosition, Quaternion.identity);

                //spawns npc if not goal
                else
                    Instantiate(npcs, randPosition, Quaternion.identity);

                //decreases each time a npc is spawned
                index--;
            }
        }

        objects = GameObject.FindGameObjectsWithTag("Camera");
    }

    // Update is called once per frame
    void Update ()
    {
        for (int i = 0; i < size; i++)
            objects[i].transform.Translate(Vector3.forward * Time.deltaTime);
            
        goal.transform.Translate(Vector3.forward * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Camera")
        {
            col.transform.Rotate(0f, Random.Range(90f, 270f), 0f);
        }
    }
}