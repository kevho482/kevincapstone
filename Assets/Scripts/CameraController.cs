﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject[] cameras;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //camera array of all the cameras
        cameras = GameObject.FindGameObjectsWithTag("Camera");

        RaycastHit hit;
        //if there's a npc in front, switch to that guy's camera
        if (Physics.Raycast(transform.position, transform.forward, out hit))
            if(hit.collider.tag == "Camera")
            {
                for (int i = 0; i < cameras.Length; i++)
                {
                    cameras[i].gameObject.GetComponent<Camera>().enabled = false;
                }

                hit.transform.gameObject.GetComponent<Camera>().enabled = true;
            }
    }
}