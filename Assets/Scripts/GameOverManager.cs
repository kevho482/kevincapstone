﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    private CharacterController player;
    public GameObject text;

    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<CharacterController>();
        text.GetComponent<Text>().enabled = false;
    }

    private void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

        if (player.GetComponent<CharacterController>().hit)
        {
            text.GetComponent<Text>().enabled = true;
            
            Time.timeScale = 0;

            if (Input.GetKey("r"))
            {
                SceneManager.LoadScene("SampleScene");
            }
        }
    }
}